package com.example.jomonco.examples.viewmodel;

import com.example.jomonco.examples.http.Response;
import com.example.jomonco.examples.http.UserService;
import com.example.jomonco.examples.model.User;
import com.example.jomonco.examples.persistence.Database;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.Data;

@Data
public class MainActivityViewModel {

    private static final int REQUEST_SIZE = 10;

    private final Database database;
    private final UserService service;

    private Observable<List<User>> users;
    private Observable<List<User>> favorites;

    @Inject
    public MainActivityViewModel(UserService service, Database database) {
        this.service = service;
        this.database = database;
    }

    public void init(IMainActivityView view) {
        users = database.userDao().getAll().toObservable();

        favorites = database.userDao().getFavorites().toObservable();

        service.getUsers(REQUEST_SIZE)
                .map(Response::getResults)
                .doOnSuccess(iUsers -> database.userDao().insert(iUsers))
                .toObservable()
                .compose(view.bindToLifecycle())
                .subscribeOn(Schedulers.single())
                .subscribe();

        view.getFavoriteChanges()
                .observeOn(Schedulers.io())
                .doOnNext(iUser -> database.userDao().insert(iUser))
                .compose(view.bindToLifecycle())
                .subscribe();
    }
}
