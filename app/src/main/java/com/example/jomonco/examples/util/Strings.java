package com.example.jomonco.examples.util;

public class Strings {

    public static String capitalize(String source) {
        if (source == null || source.length() == 0) {
            return null;
        }

        return source.substring(0, 1).toUpperCase() + source.substring(1);
    }

}
