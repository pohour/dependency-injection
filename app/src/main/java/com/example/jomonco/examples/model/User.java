package com.example.jomonco.examples.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.jomonco.examples.util.Strings;

import lombok.Data;

@Data
@Entity
public class User {
    @NonNull
    @PrimaryKey
    private String email = "dependency.default";
    @Embedded
    private Identifier identifier;
    @Embedded(prefix = "name_")
    private Name name;
    @Embedded
    private Picture picture;
    private String gender;
    private String phone;
    private boolean favorite;

    public User() {

    }

    @Data
    public static class Name {
        private String title;
        private String first;
        private String last;

        public String formatted() {
            return Strings.capitalize(first) + " " + Strings.capitalize(first);
        }
    }

    @Data
    public static  class Identifier {
        private String name;
        private String value;
    }

    @Data
    public static class Picture {
        private String large;
        private String medium;
        private String thumbnail;
        private String nat;
    }
}
