package com.example.jomonco.examples.dependency;

import dagger.Subcomponent;

@ServiceScope
@Subcomponent(modules = { ServicesModule.class })
public interface ServicesComponent {

    @Subcomponent.Builder
    interface Builder {
        ServicesComponent build();
    }

}
