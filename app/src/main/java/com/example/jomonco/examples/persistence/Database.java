package com.example.jomonco.examples.persistence;

import android.arch.persistence.room.RoomDatabase;

import com.example.jomonco.examples.model.User;

@android.arch.persistence.room.Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class Database extends RoomDatabase {
    public abstract UserDao userDao();
}
