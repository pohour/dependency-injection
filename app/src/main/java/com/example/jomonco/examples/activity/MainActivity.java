package com.example.jomonco.examples.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.jomonco.examples.Application;
import com.example.jomonco.examples.R;
import com.example.jomonco.examples.adapter.FavoritesAdapter;
import com.example.jomonco.examples.adapter.UserAdapter;
import com.example.jomonco.examples.model.User;
import com.example.jomonco.examples.viewmodel.IMainActivityView;
import com.example.jomonco.examples.viewmodel.MainActivityViewModel;
import com.trello.rxlifecycle2.components.RxActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class MainActivity extends RxActivity implements IMainActivityView {

    private final int SNACKBAR_DURATION = 1000;

    @Inject
    public MainActivityViewModel viewModel;

    @BindView(R.id.recyler_view)
    RecyclerView usersView;

    @BindView(R.id.favorites_recycler)
    RecyclerView favoritesView;

    @BindView(R.id.context_view)
    ViewGroup contextView;

    private UserAdapter usersAdapter;
    private FavoritesAdapter favoritesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Application.getInjector()
                .inject(this);

        usersAdapter = new UserAdapter();
        favoritesAdapter = new FavoritesAdapter();

        usersView.setLayoutManager(new LinearLayoutManager(this));
        usersView.setAdapter(usersAdapter);

        favoritesView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        favoritesView.setAdapter(favoritesAdapter);

        viewModel.init(this);

        bindLayout();
    }

    @Override
    public Observable<User> getFavoriteChanges() {
        return usersAdapter.getFavoriteClicked();
    }

    private void bindLayout() {
        viewModel.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(users -> {
                    usersAdapter.setUsers(users);
                    usersAdapter.notifyDataSetChanged();
                })
                .doOnError(throwable -> Snackbar.make(contextView, throwable.getLocalizedMessage(), SNACKBAR_DURATION).show())
                .compose(bindToLifecycle())
                .subscribe();

        viewModel.getFavorites()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(users -> {
                    favoritesAdapter.setUsers(users);
                    favoritesAdapter.notifyDataSetChanged();
                })
                .doOnError(throwable -> Snackbar.make(contextView, throwable.getLocalizedMessage(), SNACKBAR_DURATION).show())
                .compose(bindToLifecycle())
                .subscribe();
    }
}
