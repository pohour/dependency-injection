package com.example.jomonco.examples.dependency;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//@Module(subcomponents = { ServicesComponent.class })
@Module
public class HttpModule {

    @Provides
    @Singleton
    public Retrofit getRetrofit(OkHttpClient okHttpClient,
                                GsonConverterFactory gsonConverterFactory,
                                RxJava2CallAdapterFactory rxJava2CallAdapterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://randomuser.me/api/")
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .build();
    }

    @Provides
    public Gson provideGson(){
        return new GsonBuilder().create();
    }

    @Provides
    public GsonConverterFactory getGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    public OkHttpClient getOkHttpClient(@HttpLoggingInterceptorBody HttpLoggingInterceptor interceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor);
        return builder.build();
    }

    @Provides
    @HttpLoggingInterceptorBody
//    @Named("HttpLoggingInterceptorBody")
    public HttpLoggingInterceptor getBodyHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @HttpLoggingInterceptorHeaders
//    @Named("HttpLoggingInterceptorHeaders")
    public HttpLoggingInterceptor getHeaderHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return interceptor;
    }

    @Provides
    public RxJava2CallAdapterFactory getRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    private @interface HttpLoggingInterceptorBody { }

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    private @interface HttpLoggingInterceptorHeaders { }
}
