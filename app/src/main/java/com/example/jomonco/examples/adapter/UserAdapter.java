package com.example.jomonco.examples.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.example.jomonco.examples.R;
import com.example.jomonco.examples.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import lombok.Setter;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private PublishSubject<User> favoriteClicked = PublishSubject.create();

    @Setter
    private List<User> users = new ArrayList<>();

    public Observable<User> getFavoriteClicked() {
        return favoriteClicked.hide();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_user, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final User user = users.get(i);
        viewHolder.bind(user, (compoundButton, checked) -> {
            user.setFavorite(checked);
            favoriteClicked.onNext(user);
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view)
        CircleImageView imageView;

        @BindView(R.id.name_text)
        TextView nameTextView;

        @BindView(R.id.email_text)
        TextView emailTextView;

        @BindView(R.id.favorite_button)
        ToggleButton favoriteButton;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        void bind(@NonNull User user, @Nullable CompoundButton.OnCheckedChangeListener favoriteCallback) {
            favoriteButton.setChecked(user.isFavorite());
            nameTextView.setText(user.getName().formatted());
            emailTextView.setText(user.getEmail());

            if (user.getPicture() != null && user.getPicture().getThumbnail() != null) {
                Glide.with(itemView.getContext())
                        .load(Uri.parse(user.getPicture().getThumbnail()))
                        .into(imageView);
            }

            if (favoriteCallback != null) {
                favoriteButton.setOnClickListener(view -> {
                    if (view instanceof ToggleButton) {
                        ToggleButton button = (ToggleButton) view;
                        favoriteCallback.onCheckedChanged(button, button.isChecked());
                    }
                });
            }
        }
    }

}
