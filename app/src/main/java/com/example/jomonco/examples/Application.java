package com.example.jomonco.examples;

import com.example.jomonco.examples.dependency.ApplicationComponent;
import com.example.jomonco.examples.dependency.DaggerApplicationComponent;

public class Application extends android.app.Application {

    private ApplicationComponent mInjector;
    private static Application sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        mInjector = DaggerApplicationComponent.builder()
                .application(this)
                .build();
    }

    public static Application getInstance()  {
        return sInstance;
    }

    public static ApplicationComponent getInjector() {
        return getInstance().mInjector;
    }
}
