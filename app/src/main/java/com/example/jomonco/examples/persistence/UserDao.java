package com.example.jomonco.examples.persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.jomonco.examples.model.User;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user ORDER BY name_first, name_last ASC")
    Flowable<List<User>> getAll();

    @Query("SELECT * FROM user WHERE favorite = 1 ORDER BY name_first, name_last ASC")
    Flowable<List<User>> getFavorites();

    @Query("SELECT COUNT(*) from user")
    int count();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<User> users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User... users);

    @Delete
    void delete(User user);
}
