package com.example.jomonco.examples.http;

import com.example.jomonco.examples.model.Information;
import com.example.jomonco.examples.model.User;

import java.util.List;

import lombok.Data;

@Data
public class Response {
    private List<User> results;
    private Information info;
}
