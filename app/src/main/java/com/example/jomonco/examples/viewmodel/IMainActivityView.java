package com.example.jomonco.examples.viewmodel;

import com.example.jomonco.examples.model.User;
import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.Observable;

public interface IMainActivityView {

    <T> LifecycleTransformer<T> bindToLifecycle();

    Observable<User> getFavoriteChanges();

}
