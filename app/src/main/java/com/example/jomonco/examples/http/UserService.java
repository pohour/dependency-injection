package com.example.jomonco.examples.http;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {
    @GET("/api")
    Single<Response> getUsers(@Query("results") int size);
}
