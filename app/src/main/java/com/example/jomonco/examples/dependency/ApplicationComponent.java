package com.example.jomonco.examples.dependency;

import android.app.Application;
import android.content.Context;

import com.example.jomonco.examples.activity.MainActivity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, HttpModule.class, ServicesModule.class})
public interface ApplicationComponent {

//    ServicesComponent.Builder servicesComponent();

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(@ApplicationContext Context application);
        ApplicationComponent build();
    }

    void inject(MainActivity activity);
}
