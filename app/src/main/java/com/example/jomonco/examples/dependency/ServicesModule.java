package com.example.jomonco.examples.dependency;

import com.example.jomonco.examples.http.UserService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ServicesModule {

    @Provides
//    @ServiceScope
    public UserService getUserService(Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }

}
