package com.example.jomonco.examples.dependency;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.jomonco.examples.persistence.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
class ApplicationModule {

    @Singleton
    @Provides
    public Database getDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context, Database.class, "dependency.database")
                .fallbackToDestructiveMigration()
                .build();
    }

}
