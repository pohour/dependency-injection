package com.example.jomonco.examples.test.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.example.jomonco.examples.http.Response;
import com.example.jomonco.examples.http.UserService;
import com.example.jomonco.examples.model.User;
import com.example.jomonco.examples.persistence.Database;
import com.example.jomonco.examples.util.TrampolineSchedulerRule;
import com.example.jomonco.examples.viewmodel.IMainActivityView;
import com.example.jomonco.examples.viewmodel.MainActivityViewModel;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

@RunWith(MockitoJUnitRunner.class)
public class MainActivityViewModelTests {

    private Database database;

    @Rule
    public TrampolineSchedulerRule schedulerRule = new TrampolineSchedulerRule();

    @Rule
    public InstantTaskExecutorRule instantTaskRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        Context context = InstrumentationRegistry.getTargetContext();
        database = Room.inMemoryDatabaseBuilder(context, Database.class)
                .allowMainThreadQueries()
                .build();
    }

    @After
    public void tearDown() {
        database.close();
    }

    private List<User> populate(int limit, int favorites) {
        ArrayList<User> users = new ArrayList<>();

        for (int i = 0; i < limit; i++) {
            User user = new User();
            user.setEmail("user-" + i + "@unit.test.com");

            if (i < favorites) {
                user.setFavorite(true);
            }

            users.add(user);
        }

        database.userDao().insert(users);
        return users;
    }

    @Test
    public void Users_DatabaseHasInfo_ReturnsLimit() {
        final int limit = 20;
        populate(limit, 0);

        Response response = new Response();
        response.setResults(Collections.emptyList());

        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUsers(Mockito.anyInt())).thenReturn(Single.just(response));

        final BehaviorSubject<ActivityEvent> lifeCycleEvents = BehaviorSubject.create();

        IMainActivityView view = Mockito.mock(IMainActivityView.class);
        Mockito.when(view.getFavoriteChanges()).thenReturn(Observable.empty());
        Mockito.when(view.bindToLifecycle()).thenReturn(RxLifecycleAndroid.bindActivity(lifeCycleEvents));

        MainActivityViewModel viewModel = new MainActivityViewModel(userService, database);
        viewModel.init(view);
        viewModel.getUsers()
                .test()
                .assertValue(users -> users.size() == limit);
    }

    @Test
    public void Users_DatabaseHasNoUsers_ReturnsZero() {
        Response response = new Response();
        response.setResults(Collections.emptyList());

        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUsers(Mockito.anyInt())).thenReturn(Single.just(response));

        final BehaviorSubject<ActivityEvent> lifeCycleEvents = BehaviorSubject.create();

        IMainActivityView view = Mockito.mock(IMainActivityView.class);
        Mockito.when(view.getFavoriteChanges()).thenReturn(Observable.empty());
        Mockito.when(view.bindToLifecycle()).thenReturn(RxLifecycleAndroid.bindActivity(lifeCycleEvents));

        MainActivityViewModel viewModel = new MainActivityViewModel(userService, database);
        viewModel.init(view);
        viewModel.getUsers()
                .test()
                .assertValue(users -> users.size() == 0);
    }

    @Test
    public void Favorites_DatabaseHasFavorites_ReturnsLimit() {
        int favorites = 5;
        populate(10, favorites);

        Response response = new Response();
        response.setResults(Collections.emptyList());

        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUsers(Mockito.anyInt())).thenReturn(Single.just(response));

        final BehaviorSubject<ActivityEvent> lifeCycleEvents = BehaviorSubject.create();

        IMainActivityView view = Mockito.mock(IMainActivityView.class);
        Mockito.when(view.getFavoriteChanges()).thenReturn(Observable.empty());
        Mockito.when(view.bindToLifecycle()).thenReturn(RxLifecycleAndroid.bindActivity(lifeCycleEvents));

        MainActivityViewModel viewModel = new MainActivityViewModel(userService, database);
        viewModel.init(view);
        viewModel.getFavorites()
                .test()
                .assertValue(users -> users.size() == favorites);
    }

    @Test
    public void Favorites_DatabaseHasNoFavorites_ReturnsZero() {
        populate(10, 0);

        Response response = new Response();
        response.setResults(Collections.emptyList());

        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUsers(Mockito.anyInt())).thenReturn(Single.just(response));

        final BehaviorSubject<ActivityEvent> lifeCycleEvents = BehaviorSubject.create();

        IMainActivityView view = Mockito.mock(IMainActivityView.class);
        Mockito.when(view.getFavoriteChanges()).thenReturn(Observable.empty());
        Mockito.when(view.bindToLifecycle()).thenReturn(RxLifecycleAndroid.bindActivity(lifeCycleEvents));

        MainActivityViewModel viewModel = new MainActivityViewModel(userService, database);
        viewModel.init(view);
        viewModel.getFavorites()
                .test()
                .assertValue(users -> users.size() == 0);
    }

    @Test
    public void SaveFavorite_SaveUserClicked_RefreshesFavoriteList() {
        List<User> users = populate(10, 0);

        User favoriteUser = users.get(0);
        favoriteUser.setFavorite(true);

        Response response = new Response();
        response.setResults(Collections.emptyList());

        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.getUsers(Mockito.anyInt())).thenReturn(Single.just(response));

        final BehaviorSubject<ActivityEvent> lifeCycleEvents = BehaviorSubject.create();

        IMainActivityView view = Mockito.mock(IMainActivityView.class);
        Mockito.when(view.getFavoriteChanges()).thenReturn(Observable.just(favoriteUser));
        Mockito.when(view.bindToLifecycle()).thenReturn(RxLifecycleAndroid.bindActivity(lifeCycleEvents));

        MainActivityViewModel viewModel = new MainActivityViewModel(userService, database);
        viewModel.init(view);
        viewModel.getFavorites()
                .test()
                .assertValue(iUsers -> iUsers.size() == 1);
    }

}
